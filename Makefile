OSTYPE = $(shell echo $$OSTYPE)
BASE_DIR := $(shell pwd)
APP_NAME = "geodata-parser"

# I have set mac as default build target because currently I'm using OSX\
If you want to compile to other OS use this format 'make build-OS_NAME'
build: build-mac

build-linux:
	GOOS=linux GOARCH=amd64 go build -v -o bin/${APP_NAME}-linux

build-mac:
	GOOS=darwin GOARCH=amd64 go build -v -o bin/${APP_NAME}-mac

build-windows:
	GOOS=windows GOARCH=amd64 go build -v -o bin/${APP_NAME}-windows

test:
	go test -v ./...

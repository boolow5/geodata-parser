package utils_test

import (
	"os"
	"path/filepath"
	"strings"
	"testing"

	"bitbucket.org/boolow5/geodata-parser/models"
	"bitbucket.org/boolow5/geodata-parser/utils"
)

func TestLoadJSONFile(t *testing.T) {
	path := filepath.Join("..", "data", "office.json")
	output := models.Record{}
	err := utils.LoadJSONFile(path, &output)
	if err != nil {
		t.Errorf("should not return error but returned %v", err)
	}
	if output.Coordinates.Lat == 0 {
		t.Error("latitude should not be zero")
	}
	if output.Coordinates.Long == 0 {
		t.Error("longitude should not be zero")
	}
}

func TestSaveToJSONFile(t *testing.T) {
	content := struct {
		Message string `json:"msg"`
	}{
		Message: "hello world",
	}
	path := "hello.json"
	// saved without error?
	err := utils.SaveToJSONFile(path, &content)
	if err != nil {
		t.Errorf("error should be nil but found '%s'", err.Error())
	}
	// file is created?
	if _, err := os.Stat(path); os.IsNotExist(err) {
		t.Errorf("file should exist but fournd error '%s'", err.Error())
	}
	// delete file
	if err := os.Remove(path); err != nil {
		t.Errorf("failed to remove test file, error '%s'", err.Error())
	}
}

func TestProcessLines(t *testing.T) {
	path := filepath.Join("..", "data", "geoData.csv")
	var columns []string
	utils.ProcessLines(path, func(line string) {
		// check the first line has columns
		if len(columns) == 0 {
			line = strings.Replace(line, "\"", "", -1)
			columns = strings.Split(line, ",")
			if len(columns) == 0 {
				t.Error("columns should be at least 3 but found 0")
				return
			}
			if columns[0] != "id" {
				t.Errorf("first column should be 'id' instead found '%s'", columns[0])
			}
		} else { // lines after the first line
			columns = strings.Split(line, ",")
			if len(columns) != 3 {
				t.Errorf("expected at least 3 but found %d", len(columns))
				return
			}
		}
	})
}

func TestGetFileName(t *testing.T) {
	testTables := []struct {
		AbsPath          string
		ExpectedFileName string
		Separator        string
	}{
		{
			AbsPath: "/User/bolow/hello.txt", ExpectedFileName: "hello.txt", Separator: "", // normal unix absolute path and default seperator on OSX
		},
		{
			AbsPath: "C:\\Users\\Downloads\\hello.txt", ExpectedFileName: "hello.txt", Separator: "\\", // windows absolute path
		},
	}

	for _, table := range testTables {
		if fileName := utils.GetFileName(table.AbsPath, table.Separator); fileName != table.ExpectedFileName {
			t.Errorf("expected file name %s but found %s", table.ExpectedFileName, fileName)
		}
	}
}

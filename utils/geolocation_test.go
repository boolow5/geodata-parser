package utils_test

import (
	"fmt"
	"testing"

	"bitbucket.org/boolow5/geodata-parser/utils"
)

func TestDistanceInMeters(t *testing.T) {
	testTable := []struct {
		lat1             float64
		long1            float64
		lat2             float64
		long2            float64
		expectedDistance float64
	}{
		{37.8667538, -122.25909899999999, 52.09791479999999808, 5.11686619999999959, 8799796.861836819},
		{52.07949729999999988, 5.13260109999999958, 48.8910475, 2.338504, 406309.49444520974},
		{52.5497, 13.41931, 52.25577359999999771, 6.18898640000000011, 491942.11949074187},
	}
	for _, table := range testTable {
		distance := utils.DistanceInMeters(table.lat1, table.long1, table.lat2, table.long2)
		if distance != table.expectedDistance {
			fmt.Println("distance:", distance)
			t.Errorf("Incorrect distance '%.10f' expected '%.10f': equal? %v", distance, table.expectedDistance, distance == table.expectedDistance)
		}
	}
}

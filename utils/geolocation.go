package utils

import (
	"math"
)

const (
	// EarthRadius in meters
	EarthRadius = 6378137
)

// convert degrees to radian
func toRadian(num float64) float64 {
	return (num * math.Pi) / 180
}

// haversin(θ) function
func haversin(theta float64) float64 {
	return math.Pow(math.Sin(theta/2), 2)
}

// DistanceInMeters returns distance between two coordinates
func DistanceInMeters(lat1, long1, lat2, long2 float64) float64 {
	// convert from degrees to radian
	lat1 = toRadian(lat1)
	long1 = toRadian(long1)
	lat2 = toRadian(lat2)
	long2 = toRadian(long2)

	// calculate
	h := haversin(lat2-lat1) + math.Cos(lat1)*math.Cos(lat2)*haversin(long2-long1)

	return 2 * EarthRadius * math.Asin(math.Sqrt(h))
}

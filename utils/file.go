package utils

import (
	"bufio"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const (
	/* If you have problems understanding UNIX file permission,
	I suggest you use http://permissions-calculator.org/
	Which allows you to generate the permission octal code you want.

	The leading zero is important, it makes the number Octal (base-8), instead of Decimal (base-10)
	*/

	// FileReadWritePermissionForAll is UNIX permission to allow everyone to read and write to the file
	FileReadWritePermissionForAll = 0666
	// FileReadWritePermissionForUserGroup is UNIX permission to allow group & user to read and write to the file
	FileReadWritePermissionForUserGroup = 0660
	// FileReadWritePermissionForUserOnly is UNIX permission to allow user only to read and write to the file
	FileReadWritePermissionForUserOnly = 0600
)

// LoadJSONFile loads and unmarshalls json file to the output variable
func LoadJSONFile(fullPath string, output interface{}) error {
	data, err := ioutil.ReadFile(fullPath)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, output)
	if err != nil {
		return err
	}
	return nil
}

// SaveToJSONFile marshals input data and saves to json file
func SaveToJSONFile(fullPath string, input interface{}, options ...string) (err error) {
	var data []byte
	var prettify bool
	for _, option := range options {
		switch option {
		case "prettify":
			prettify = true
		}
	}
	if prettify {
		data, err = json.MarshalIndent(input, "", "\t")
	} else {
		data, err = json.Marshal(input)
	}
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fullPath, data, os.FileMode(FileReadWritePermissionForAll))
	if err != nil {
		return err
	}
	return nil
}

// ProcessLines takes file path, and processor callback which is used for processing each line
func ProcessLines(path string, processorFunc func(line string)) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal("file error: " + err.Error())
		return
	}
	defer file.Close()
	// TODO: scanner can read 65536 character maximum, in the future if you intend to read longer lines, change this.
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		processorFunc(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

// GetFileName extracts file name from absolute path, if empty separator is passed it uses current sytem's as default
func GetFileName(absPath, sep string) string {
	if len(sep) == 0 {
		sep = string(filepath.Separator)
	}
	parts := strings.Split(absPath, sep)
	if len(parts) > 0 {
		return parts[len(parts)-1:][0]
	}
	return absPath
}

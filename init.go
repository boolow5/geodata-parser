package main

import (
	"flag"
	"fmt"
	"path/filepath"

	"bitbucket.org/boolow5/geodata-parser/models"
)

const (
	sourceFile          = "file"
	sourceDatabase      = "database"
	shortSourceFile     = "f"
	shortSourceDatabase = "d"
)

var (
	outputFile    string
	isCompactFile bool
)

func init() {
	fmt.Println("geocode-parser")
	dataSourcePtr := flag.String("data-source", sourceFile, fmt.Sprintf("%s|%s|%s|%s", sourceFile, sourceDatabase, shortSourceFile, shortSourceDatabase))
	shortDataSourcePtr := flag.String("ds", "", fmt.Sprintf("%s|%s|%s|%s", sourceFile, sourceDatabase, shortSourceFile, shortSourceDatabase))

	defautlFile := filepath.Join(models.CurrentDir, "data", "geoData.csv")

	sourceURIPtr := flag.String("uri", defautlFile, fmt.Sprintf("%s|%s|%s|%s", sourceFile, sourceDatabase, shortSourceFile, shortSourceDatabase))

	outputFilePtr := flag.String("output", "", "output.json")

	compactFile := flag.Bool("compact", false, "")

	flag.Parse()

	if len(*shortDataSourcePtr) > 0 {
		models.DataSourceType = *shortDataSourcePtr
	} else {
		models.DataSourceType = *dataSourcePtr
	}
	if len(*outputFilePtr) > 0 {
		outputFile = *outputFilePtr
	}
	models.DataSourceURI = *sourceURIPtr
	isCompactFile = *compactFile
}

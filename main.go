package main

import (
	"fmt"

	"bitbucket.org/boolow5/geodata-parser/models"
	"bitbucket.org/boolow5/geodata-parser/utils"
)

func main() {
	err := models.Init("data")
	if err != nil {
		panic(fmt.Sprintf("Failed to initialize models. Reason: %v", err))
	}
	topClosest := models.GetTopRecords(true)
	topFarthest := models.GetTopRecords(false)
	if len(outputFile) == 0 {
		fmt.Println("____________________________________\nTop 5 closest records to office:")
		fmt.Println("     ID\t\tDistance")
		for _, record := range topClosest {
			fmt.Printf("%7d\t\t%.2f km\n", record.ID, record.OfficeDistance)
		}

		fmt.Println("____________________________________\nTop 5 farthest records to office:")
		fmt.Println("     ID\t\tDistance")
		for _, record := range topFarthest {
			fmt.Printf("%7d\t\t%.2f km\n", record.ID, record.OfficeDistance)
		}
	} else {
		data := map[string]interface{}{
			"top_nearest":  topClosest,
			"top_farthest": topFarthest,
		}
		option := "prettify"
		if isCompactFile {
			option = ""
		}
		err = utils.SaveToJSONFile(outputFile, data, option)
		if err != nil {
			fmt.Println("Failed to save. Error:", err)
			return
		}
		fmt.Printf("Successfully saved to '%s'\n", outputFile)
	}
	fmt.Println("Done!")
}

# GoeData parser

GeoData parser is written in Go, and allows you to load data form CSV file `data/geoData.csv`, in the future it should support other data sources.

This tool calculates distance between records using their coordinates against office record coordinates stored in `data/office.json`.

The office data is not hardcoded, to make it easy for those who have access to only the binary to easily modify their target record (office record).

## Build

If you are using OSX you can simply run `make build` but if you are using another OS or cross compiling for another Operating System use the following commands:
```bash
# for Linux
make build-linux
# for Windows
make build-windows
```

## Run

To run the executable, use the following commands:
```bash
# directly run
go run *.go
# but if you want to run precompiled binaries, use the following

# for OSX
./bin/geodata-parser-mac

# for Linux
./bin/geodata-parser-linux

# for Windows
./bin/geodata-parser-windows
```

## Output

This tool prints results to the terminal, but if you want the output as JSON file, use the following parameter `-output=<FILE_PATH>.json` replace `FILE_PATH` with the file name and path you want to save output data. 

If you want the JSON file as compact - without indentation - you can use `-compact` parameter.

## Run unit tests

Test files are contained within each package directory. To run tests for sub-modules use the following:

```bash
go test -v ./...
# or simply
make test
```

## TODO

This tool currently process CSV files as input source, other data sources such as database are not implemented yet.

## Disclaimer

Face palm ahead! :)

I didn't work with a team for some time, if you see something stupid don't tell me I didn't warn you! 
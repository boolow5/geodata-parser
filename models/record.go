package models

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/boolow5/geodata-parser/utils"
)

var records []*Record

// Record represents a single record in the listings
type Record struct {
	ID             int         `json:"id"`
	Coordinates    Coordinates `json:"coordinates"`
	OfficeDistance float64     `json:"office_distance"`
}

// Coordinates represents a point on earth
type Coordinates struct {
	Lat  float64 `json:"lat"`
	Long float64 `json:"long"`
}

// FromCSV loads data form comma seperated string
func (r *Record) FromCSV(colNames []string, line string) (err error) {
	// give default column names if passed nil
	if colNames == nil {
		colNames = []string{"id", "lat", "lng"}
	}
	// split line to parts, I could use csv package but this is little faster
	parts := strings.Split(line, ",")

	// don't accept less data parts than expected columns
	if len(parts) > len(colNames) {
		return fmt.Errorf("expected %d columns but input contains %d columns", len(colNames), len(parts))
	}

	// temp will hold record data until processing loop ends and no error encountered
	var temp Record
	var errMessages []string

	// to save some lines of code
	appendError := func(e error) {
		if e != nil {
			errMessages = append(errMessages, e.Error())
		}
	}

	// match columns with record fields
	for i := 0; i < len(colNames); i++ {
		switch colNames[i] {
		case "id":
			temp.ID, err = strconv.Atoi(parts[i])
			appendError(err)
		case "lat":
			temp.Coordinates.Lat, err = strconv.ParseFloat(parts[i], 64)
			appendError(err)
		case "lng":
			temp.Coordinates.Long, err = strconv.ParseFloat(parts[i], 64)
			appendError(err)
		case "distance":
			temp.Coordinates.Long, err = strconv.ParseFloat(parts[i], 64)
			appendError(err)
		default:
			// ignore unknown columns
		}
	}
	if len(errMessages) == 0 {
		r.ID = temp.ID
		r.Coordinates = temp.Coordinates
		r.OfficeDistance = temp.OfficeDistance
		return nil
	}
	return fmt.Errorf("%d errors occured: %v", len(errMessages), strings.Join(errMessages, ", "))
}

// UpdateOfficeDistance calculates and sets distance between this record and the office
func (r *Record) UpdateOfficeDistance() {
	r.OfficeDistance = CalculateDistance(r.Coordinates, Office.Coordinates)
}

// LoadRecords loads records from the source type and URI
func LoadRecords(sourceType, sourceURI string) (count int, err error) {
	// FILE SOURCE HANDLING
	if sourceType == fileSource {
		firstLine := true
		utils.ProcessLines(sourceURI, func(line string) {
			if firstLine {
				firstLine = false
				return
			}
			line = strings.TrimSpace(line)
			if len(line) > 0 {
				count++
				lineToRecord(line)
			}
		})
	} else if sourceType == databaseSource { // DATABASE SOURCE HANDLING
		// TODO: implement this when database data source is added
		return count, fmt.Errorf("database source is not implemented yet")
	}
	return count, nil
}

func lineToRecord(line string) {
	r := Record{}
	err := r.FromCSV(nil, line)
	if err != nil {
		return
	}
	records = append(records, &r)
}

func updateRecordDistances() {
	for i := 0; i < len(records); i++ {
		records[i].UpdateOfficeDistance()
		// fmt.Printf("%10d:\tdistance: %f\n", records[i].ID, records[i].OfficeDistance)
	}
}

// CalculateDistance returns distance between two coordinates in kilometers
func CalculateDistance(coords1, coords2 Coordinates) (distance float64) {
	return utils.DistanceInMeters(coords1.Lat, coords1.Long, coords2.Lat, coords2.Long) / 1000
}

// GetTopRecords returns top records either top closest or top farthest
func GetTopRecords(closest bool) []*Record {
	if len(records) < 5 {
		return nil
	}
	if closest {
		return records[:5]
	}
	return records[len(records)-5:]
}

package models_test

import (
	"path/filepath"
	"testing"

	"bitbucket.org/boolow5/geodata-parser/models"
)

func TestFromCSV(t *testing.T) {
	line := "7818,37.8667538,-122.25909899999999"
	record := models.Record{}
	record.FromCSV(nil, line)
	if record.ID != 7818 {
		t.Errorf("Record ID should be '7818' but found '%d'", record.ID)
	}
	if record.Coordinates.Lat != 37.8667538 {
		t.Errorf("Record latitude expected '37.8667538' but found '%f'", record.Coordinates.Lat)
	}
	if record.Coordinates.Long != -122.25909899999999 {
		t.Errorf("Record longitude expected '-122.25909899999999' but found '%f'", record.Coordinates.Long)
	}
}

func TestLoadRecords(t *testing.T) {
	count, err := models.LoadRecords("file", filepath.Join("..", "data", "geoData.csv"))
	if err != nil {
		t.Errorf("there should not be error but found '%s'", err.Error())
	}
	if count < 1 {
		t.Errorf("there should more than one record but found '%d' records", count)
	}
}

func TestGetTopRecords(t *testing.T) {
	err := models.Init(filepath.Join("..", "data"))
	if err != nil {
		t.Errorf("should initialize without error but got this error %v", err)
	}
	topNearest := models.GetTopRecords(true)
	// should be 5 items
	if len(topNearest) != 5 {
		t.Errorf("there should return 5 record but found '%d' records", len(topNearest))
	}
	// first item should have least distance
	leastDistance := topNearest[0].OfficeDistance
	for i := 0; i < len(topNearest); i++ {
		if topNearest[i].OfficeDistance < leastDistance {
			t.Errorf("record %d should have distance less than %f but found %f", topNearest[i].ID, leastDistance, topNearest[i].OfficeDistance)
		}
	}
	// last item should have largest distance
	lastIndex := len(topNearest) - 1
	largestDistance := topNearest[lastIndex].OfficeDistance
	for i := 0; i < len(topNearest); i++ {
		if topNearest[i].OfficeDistance > largestDistance {
			t.Errorf("record %d should have distance less than %f but found %f", topNearest[i].ID, largestDistance, topNearest[i].OfficeDistance)
		}
	}

}

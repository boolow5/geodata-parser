package models

import (
	"os"
	"path/filepath"

	"bitbucket.org/boolow5/geodata-parser/utils"
)

const (
	fileSource     = "file"
	databaseSource = "database"
)

var (
	// CurrentDir is the current running app's home directory
	CurrentDir string
	// DataSourceType is the source type database or file
	DataSourceType string
	// DataSourceURI is the link used to communicate with the datasource
	// eg. file path or database url
	DataSourceURI string
	// Office represents the record used to compare to other records
	Office Record
)

// init is the automatic init different from Init with uppercase I
func init() {
	CurrentDir, _ = filepath.Abs(filepath.Dir(os.Args[0]))
}

// Init initializes and prepares the models
func Init(dataDir string) error {
	err := loadOfficeInfo(dataDir)
	if err != nil {
		return err
	}
	path := dataDir
	fileName := utils.GetFileName(DataSourceURI, "")
	// dataDir is passed use it as directory for the DataSourceURI file
	// if dir is not passed, use the DataSourceURI directory
	if len(path) == 0 {
		path = filepath.Dir(DataSourceURI)
	}
	_, err = LoadRecords(DataSourceType, filepath.Join(path, fileName))
	if err != nil {
		return err
	}
	updateRecordDistances()
	sortRecordsByDistance()
	return nil
}

func loadOfficeInfo(dir string) error {
	path := filepath.Join(dir, "office.json")
	return utils.LoadJSONFile(path, &Office)
}

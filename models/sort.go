package models

import (
	"sort"
)

// SortRecords used to impelement sort.Interface
type SortRecords []*Record

// Len is required to satisfy sort.Interface
func (sr SortRecords) Len() int {
	return len(sr)
}

// Swap is required to satisfy sort.Interface
func (sr SortRecords) Swap(i, j int) {
	sr[i], sr[j] = sr[j], sr[i]
}

// Less is required to satisfy sort.Interface
func (sr SortRecords) Less(i, j int) bool {
	return sr[i].OfficeDistance < sr[j].OfficeDistance
}

func sortRecordsByDistance() {
	sort.Sort(SortRecords(records))
}
